import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { switchMap } from 'rxjs/operators';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-personaje',
  templateUrl: './personaje.component.html',
  styleUrls: ['./personaje.component.css']
})
export class PersonajeComponent  {

  data;
  urls;
  vehicles = [];

  constructor(private http: Http, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {  this.urls = params.url;  })

  this.http.get(this.urls).pipe((data: any) => data).subscribe(
     (data:any) => {
       data = JSON.parse(data['_body']);
       this.data = data;
       for (var prop of this.data.vehicles) {
          this.loadVehicles(prop);
      }
   }
   );
  }

  loadVehicles(url){
    this.http.get(url).pipe((data: any) => data).subscribe(
       (data:any) => {
          data = JSON.parse(data['_body']);
          data.pelis = "";
          for (var prop of this.data.films) {
              this.loadFilsm(prop,data);
         }

          this.vehicles.push(data);
        }
     );
  }

  loadFilsm(url,datas){
    var  films;
    this.http.get(url).pipe((data: any) => data).subscribe(
       (data:any) => {
          data = JSON.parse(data['_body']);
          console.log(data);
          datas.pelis+=data.title+" ";
        }
     );
  }

}
