import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  constructor(private http: Http) { }
  httpdata;

  ngOnInit() {



    this.http.get("https://swapi.co/api/films")
    .pipe((data: any) => data)
   .subscribe(
     (data:any) => {
       data = JSON.parse(data['_body']);
       this.httpdata = data.results;
     console.log(this.httpdata);
   }
   );
  }

}
