import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule,  Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { DetallesPeliculaComponent } from './detalles-pelicula/detalles-pelicula.component';


import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {OverlayContainer} from '@angular/cdk/overlay';
import {MatCardModule} from '@angular/material/card';
import { PeliculaComponent } from './pelicula/pelicula.component';
import { PersonajeComponent } from './personaje/personaje.component';





const appRoutes: Routes = [
  { path: 'detalles-pelicula', component: DetallesPeliculaComponent },
  { path: 'personaje', component: PersonajeComponent },
  { path: 'peliculas', component: PeliculaComponent },
  { path: '',   redirectTo: '/peliculas', pathMatch: 'full' }
]

@NgModule({
  declarations: [
    AppComponent,
    DetallesPeliculaComponent,
    PeliculaComponent,
    PersonajeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
   MatButtonModule,
   MatSelectModule,
   MatIconModule,
   MatToolbarModule,
   MatCardModule,

    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true,
      }
    )

  ],
  exports: [
   RouterModule
 ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
