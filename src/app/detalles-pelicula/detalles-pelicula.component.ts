import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { switchMap } from 'rxjs/operators';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs';




@Component({
  selector: 'app-root',
  templateUrl: './detalles-pelicula.component.html',
  styleUrls: ['./detalles-pelicula.component.css']
})
export class DetallesPeliculaComponent {
  data;
  urls;
  personajes = [];

  constructor(private http: Http, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {  this.urls = params.url;  })

  this.http.get(this.urls).pipe((data: any) => data).subscribe(
     (data:any) => {
       data = JSON.parse(data['_body']);
       this.data = data;
       for (var prop of this.data.characters) {
          this.loadPersonajes(prop);
      }
   }
   );
  }

  loadPersonajes(url){
    this.http.get(url).pipe((data: any) => data).subscribe(
       (data:any) => {
          data = JSON.parse(data['_body']);
          this.personajes.push(data);
        }
     );
  }

}
